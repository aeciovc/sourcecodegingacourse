--tables com chaves
t = {}
t["x"] = 10
t["y"] = 12
print(t["x"] + t["y"])

--alternativa
t = { ["x"] = 10, ["y"] = 12 }
print(t["x"] + t["y"])


--acessando atributos com ponto .
t = {}
t.x = 10
t.y = 12
print (t.x + t["y"])


--Abreviando
t = { x = 10, y = 12 }
