
--simple function
function soma(x, y)
	return x + y
end

--function anonima
operacao = function (x, y)
	return x + y
end

--multiple returns
function operation(x, y)
	return x + y, x-y , x*y, x/y
end

print("Digite o 1º valor:")
value1 = io.read()

print("Digite o 2º valor:")
value2 = io.read()

result = soma(value1, value2)
som, sub, mul, div = operation(value1, value2)


print("\n")

print("Resultado: " .. result .. "\n\n")

print("Resultados Multiplos \n")
print("Soma: "..som.."\n")
print("Subtração: "..sub.."\n")
print("Multiplicação: "..mul.."\n")
print("Divisão: "..div.."\n")






