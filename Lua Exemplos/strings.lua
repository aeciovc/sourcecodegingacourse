
--Maiuscula e Minuscula
var = "Lua is cool language!"
print(string.upper(var), string.lower(var))

--String Size
print(string.len(var))
-- Uma sintaxe equivalente�� usando
print(#var)


--Tabela ASCII
print(string.char(84, 101, 115, 116, 101))

--Retorna o primeiro caracter
print(string.byte "Teste")

--posi��o 1 at� 3
print(string.byte("Teste", 1, 3))

--String Format
nome = "Jos� da Silva"
idade = 42
print(string.format("Nome: %s\nIdade: %d", nome, idade))

--Substrings
var = "Mais um texto chato"
print(string.sub(var, 1, 4))
print(string.sub(var, 1, -7))

--Busca em Strings
print(string.find( "Mais um texto chato", "texto"))
print(string.match("Mais um texto chato", "texto"))

--Replace Strings
print(string.gsub("Perl�� uma �tima linguagem","Perl","Lua"))
