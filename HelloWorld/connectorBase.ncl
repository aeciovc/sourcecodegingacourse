<ncl id="connectorBase" 
xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.ncl.org.br/NCL3.0/EDTVProfile http://www.ncl.org.br/NCL3.0/profiles/NCL30EDTV.xsd">

<head>

  <connectorBase>
  	
  	  <causalConnector id="aoIniciarSetar">
      	  <connectorParam name="var"/>
        	<simpleCondition role="onBegin" />
     		<simpleAction role="set" value="$var" max="unbounded" qualifier="par"/>
      </causalConnector>
  	
  	  <causalConnector id="onEndAttributionStartNStopN">
         <simpleCondition role="onEndAttribution"/>
         	<compoundAction operator="seq">
				<simpleAction role="start" max="unbounded" qualifier="seq" />
				<simpleAction role="stop" max="unbounded" qualifier="seq" />
			</compoundAction>
     </causalConnector> 
     
     <causalConnector id="onEndAttributionStartN">
         <simpleCondition role="onEndAttribution"/>
         	<compoundAction operator="seq">
				<simpleAction role="start" max="unbounded" qualifier="seq" />
			</compoundAction>
     </causalConnector> 
    
    <causalConnector id="onEndAttributionStopN">
         <simpleCondition role="onEndAttribution"/>
         	<compoundAction operator="seq">
				<simpleAction role="stop" max="unbounded" qualifier="seq" />
			</compoundAction>
     </causalConnector> 
            	
  	
  	 <causalConnector id="onEndAttributionStart">
         <simpleCondition role="onEndAttribution"/>
         <simpleAction role="start" />
     </causalConnector> 
            	
  	<causalConnector id="onBeginSet">
		<connectorParam name="var"/>
		<simpleCondition role="onBegin"/>
		<simpleAction role="set" value="$var"/>
	</causalConnector>
		
    <causalConnector id="onBeginStart">
      <simpleCondition role="onBegin"/>
      <simpleAction role="start" max="unbounded" qualifier="seq"/>
    </causalConnector>
    
    <causalConnector id="onBeginStartStop">
      <simpleCondition role="onBegin"/>
      	<compoundAction operator="seq">
			<simpleAction role="start" />
      		<simpleAction role="stop" />
		</compoundAction>
    </causalConnector>

    <causalConnector id="onEndStart">
      <simpleCondition role="onEnd"/>
      <simpleAction role="start" max="unbounded" qualifier="seq"/>
    </causalConnector>

    <causalConnector id="onEndStop">
      <simpleCondition role="onEnd"/>
      <simpleAction role="stop" max="unbounded" qualifier="par"/>
    </causalConnector>

    <causalConnector id="onEndAbort">
      <simpleCondition role="onEnd"/>
      <simpleAction role="abort" max="unbounded" qualifier="par"/>
    </causalConnector>

    <causalConnector id="onKeySelectionStartStop">
      <connectorParam name="keyCode"/>
      <simpleCondition role="onSelection" key="$keyCode" max="unbounded"
                       qualifier="or" />
      <compoundAction operator="seq">
        <simpleAction role="stop" max="unbounded" qualifier="seq"/>
        <simpleAction role="start" max="unbounded" qualifier="seq"/>
      </compoundAction>
    </causalConnector>

    <causalConnector id="onKeySelectionSetResizeStartStop">
      <connectorParam name="keyCode" />
      <connectorParam name="var" />
      <simpleCondition role="onSelection" key="$keyCode"/>
      <compoundAction operator="seq">
        <simpleAction role="stop" max="unbounded" qualifier="seq"/>
        <simpleAction role="set" value="$var" max="unbounded" qualifier="seq"/>
        <simpleAction role="start" max="unbounded" qualifier="seq"/>
      </compoundAction>
    </causalConnector>


  </connectorBase>

</head>

</ncl>