<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="appCongresso" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>

				
		<regionBase>
				
				<region id="rgVideo" width="100%" height="100%" zIndex="1"/>
				
				<region id="rgIcone" width="29.5%" height="6.5%"  top="20%" left="70%" zIndex="2"  />
					
				<region id="rgBackgroundDireita"  width="85" height="100%"  left="100%" zIndex="2"  />
				<region id="rgBackgroundEsquerda" width="85" height="100%" zIndex="2"/>
			
				<region id="rgOpcao1" width="121" height="41" top="30%" left="30" zIndex="3" />
				<region id="rgOpcao2" width="121" height="41" top="40%" left="30" zIndex="3" />
				<region id="rgOpcao3" width="121" height="41" top="50%" left="30" zIndex="3" />
				
				<region id="rgEnquete" width="190" height="175" top="30%" left="80%" zIndex="3" />
		
		</regionBase>
		
		<descriptorBase>
						
			<descriptor id="dsVideo" region="rgVideo"/>
			
			<descriptor id="dsIcone" region="rgIcone" />
			
			<descriptor id="dsBackgroundDireita" region="rgBackgroundDireita"/>
			<descriptor id="dsBackgroundEsquerda" region="rgBackgroundEsquerda"   />
												
			<descriptor id="dOpcao1" region="rgOpcao1" focusIndex="1" moveDown="2" moveUp="3" focusSrc="media/enquetes_on.png" focusBorderWidth="0" />
			<descriptor id="dOpcao2" region="rgOpcao2" focusIndex="2" moveDown="3" moveUp="1" focusSrc="media/tmail_on.png" focusBorderWidth="0" />
			<descriptor id="dOpcao3" region="rgOpcao3" focusIndex="3" moveDown="1" moveUp="2" focusSrc="media/vendas_on.png" focusBorderWidth="0" />
			
			<descriptor id="dsEnquete" region="rgEnquete" />
		</descriptorBase>
						
		<connectorBase>
			<importBase alias="connectors" documentURI="connectorBase.ncl"/>
		</connectorBase>
		
	</head>
	
	<body>


	</body>
</ncl>
