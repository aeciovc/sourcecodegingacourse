<?xml version="1.0" encoding="ISO-8859-1"?>
<ncl id="appEnquete" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<regionBase>
			<region id="rgBackground"  width="100%" height="100%">
				
				<region id="rgVideo" width="100%" height="100%" zIndex="1"/>
				<region id="rgIcone" width="215" height="35" left="65%" top="2%" zIndex="3"/>
				<region id="rgFundo" width="100%" height="100%" zIndex="2" />
				
				<region id="rgPergunta" width="540" height="79" top="400" left="100" zIndex="2" />
				<region id="rgOpcoes" width="491" height="48" top="500" left="100" zIndex="2" />
				<region id="rgResposta" width="540" height="79" top="400" left="100" zIndex="2" />
				
			</region>
		</regionBase>
		
		<descriptorBase>
			<descriptor id="dsBackground" region="rgBackground"/>
			
			<descriptor id="dsIcone" region="rgIcone"/>
			<descriptor id="dsVideo" region="rgVideo"/>
			<descriptor id="dsFundo" region="rgFundo"/>
			
			<descriptor id="dsPergunta" region="rgPergunta"/>
			<descriptor id="dsOpcoes" region="rgOpcoes"/>
			<descriptor id="dsRespostaCerta" region="rgResposta"/>
			<descriptor id="dsRespostaErrada" region="rgResposta"/>
			
			
		</descriptorBase>
		
		<connectorBase>
			<importBase alias="connectors" documentURI="connectorBase.ncl"/>
		</connectorBase>
		
	</head>
	
	<body>
		
		<!-- Ponto de Entrada Inicial -->
		<port id="entrada" component="video" />
		
		
		<media descriptor="dsIcone" id="icone" src="media/interativo.png"/>
		
		<media descriptor="dsVideo" id="video" src="media/video.mpg">
			<property name="bounds"/>
		</media>	
		
		<media descriptor="dsFundo" id="fundo" src="media/fundo.png"/>
		
		<media descriptor="dsPergunta" id="pergunta" src="media/pergunta.png"/>
		<media descriptor="dsOpcoes" id="opcoes" src="media/opcoes.png"/>
		<media descriptor="dsRespostaCerta" id="respostaCerta" src="media/respostaCerta.png"/>
		<media descriptor="dsRespostaErrada" id="respostaErrada" src="media/respostaErrada.png"/>
		
			
		<link xconnector="connectors#onBeginStart">
			<bind component="video" role="onBegin" />
			<bind component="icone" role="start"/>
		</link>
			
		<link xconnector="connectors#onKeySelectionSetResizeStartStop">
			<bind component="icone" role="onSelection">
				<bindParam name="keyCode" value="RED"/>
			</bind>
			
			<bind component="video" interface="bounds" role="set">
				<bindParam name="var" value="338, 52, 50%, 50%"/>
			</bind>
			
			<bind component="fundo" role="start"/>
			<bind component="opcoes" role="start"/>
			<bind component="pergunta" role="start"/>
			<bind component="icone" role="stop" />
		</link>
				
		<link xconnector="connectors#onKeySelectionStartStop">
			<bind component="pergunta" role="onSelection">
				<bindParam name="keyCode" value="RED"/>
			</bind>

			<bind component="pergunta" role="stop"/>
			<bind component="opcoes" role="stop"/>
			<bind component="respostaCerta" role="start"/>
		</link>
		
		<link xconnector="connectors#onKeySelectionStartStop">
			<bind component="pergunta" role="onSelection">
				<bindParam name="keyCode" value="GREEN"/>
			</bind>

			<bind component="pergunta" role="stop"/>
			<bind component="opcoes" role="stop"/>
			<bind component="respostaErrada" role="start"/>
		</link>
		
		<link xconnector="connectors#onKeySelectionStartStop">
			<bind component="pergunta" role="onSelection">
				<bindParam name="keyCode" value="YELLOW"/>
			</bind>

			<bind component="pergunta" role="stop"/>
			<bind component="opcoes" role="stop"/>
			<bind component="respostaErrada" role="start"/>
		</link>
		
		<link xconnector="connectors#onKeySelectionStartStop">
			<bind component="pergunta" role="onSelection">
				<bindParam name="keyCode" value="BLUE"/>
			</bind>

			<bind component="pergunta" role="stop"/>
			<bind component="opcoes" role="stop"/>
			<bind component="respostaErrada" role="start"/>
		</link>
	
		
	</body>
</ncl>
